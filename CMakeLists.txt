cmake_minimum_required(VERSION 3.16)
project(template C CXX)

# OPTIONS
option(SANITIZE "Compile with address sanitizer" off)
option(TIDY "Clang-tidy" off)

set(CMAKE_EXPORT_COMPILE_COMMANDS "ON")

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 14)

# glad
include_directories(${PROJECT_SOURCE_DIR}/lib/vendor/glad/include)
add_library(glad ${PROJECT_SOURCE_DIR}/lib/vendor/glad/src/glad.c)
target_compile_options(glad PUBLIC "-w;")

# glfw
include_directories(${PROJECT_SOURCE_DIR}/lib/vendor/glfw/include)
add_subdirectory(${PROJECT_SOURCE_DIR}/lib/vendor/glfw)
target_compile_options(glfw PUBLIC "-w")

# source
file(GLOB_RECURSE ${PROJECT_NAME}_sources ${PROJECT_SOURCE_DIR}/src/*.cpp ${PROJECT_SOURCE_DIR}/src/*/*.cpp)
add_executable(${PROJECT_NAME} ${${PROJECT_NAME}_sources})
target_compile_options(${PROJECT_NAME} PUBLIC "-Wall;-Wextra;-pedantic")
target_link_libraries(${PROJECT_NAME} PUBLIC glfw GL glad dl)

enable_testing()

if(SANITIZE)
  message("Sanitizing Address...")
  set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "-fno-omit-frame-pointer")
  set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS "-fsanitize=address")
endif()

if(TIDY)
  message("Using clang-tidy...")
  set(CMAKE_CXX_CLANG_TIDY clang-tidy -header-filter=.,-checks=-*,readability-*)
endif()

#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/assets DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
